import jwt_decode from 'jwt-decode';

const STORAGE_ACCESS_TOKEN = 'access'
const STORAGE_REFRESH_TOKEN = 'refresh'

export function loadTokens() {
  const access = localStorage.getItem(STORAGE_ACCESS_TOKEN);
  const refresh = localStorage.getItem(STORAGE_REFRESH_TOKEN);

  const ok = [access, refresh].every(token => {
    return token && token !== 'undefined'
  })

  return ok ? { access, refresh } : undefined
}

export function saveTokens(tokens) {
  localStorage.setItem(STORAGE_ACCESS_TOKEN, tokens.access)
  localStorage.setItem(STORAGE_REFRESH_TOKEN, tokens.refresh)
}

export function dropTokens() {
  localStorage.removeItem(STORAGE_ACCESS_TOKEN)
  localStorage.removeItem(STORAGE_REFRESH_TOKEN)
}

export function getExpiration(token) {
  return decodeToken(token).exp * 1000 - Date.now()
}

export function decodeToken(token) {
  return jwt_decode(token)
}
