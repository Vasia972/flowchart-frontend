import { createRouter, createWebHistory } from 'vue-router';
import AlbumDetail from '@/pages/AlbumDetail';
import UserRegistration from '@/pages/UserRegistration';
import UserLogin from '@/pages/UserLogin';
import LikedAlbums from '@/pages/LikedAlbums';

export default createRouter({
  history: createWebHistory(),
  routes: [
    { name: 'album', path: '/albums/:id', component: AlbumDetail },
    { name: 'random', path: '/random', alias: '/', component: AlbumDetail },
    { name: 'register', path: '/register', component: UserRegistration },
    { name: 'login', path: '/login', component: UserLogin },
    { name: 'likes', path: '/likes', component: LikedAlbums },
  ]
})
