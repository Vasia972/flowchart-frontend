function padWithZeroes(x) {
  if (String(x).length === 1) {
    return '0' + x
  }
  return x
}
export function formatTrackTime(duration) {
  const minutes = Math.floor(duration/60)
  const seconds = Math.floor(duration % 60)
  return `${padWithZeroes(minutes)}:${padWithZeroes(seconds)}`
}
