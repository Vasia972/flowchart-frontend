import { MEDIA_HOST } from '@/config';

export function makeMediaUrl(path) {
  return MEDIA_HOST + path
}
