import { createStore } from 'vuex';
import AlbumsStore from './albums'
import AuthStore from './auth'

export default createStore({
  modules: {
    albums: AlbumsStore,
    auth: AuthStore,
  },
})
