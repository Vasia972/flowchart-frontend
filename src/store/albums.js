import { flowChartApi } from '@/api';

export default {
  namespaced: true,
  state() {
    return {
      id: null,
      name: null,
      cover: null,
      liked: null,
      tracks: [],
      children: [],

      likedAlbums: []
    };
  },
  mutations: {
    setAlbum(state, payload) {
      state.id = payload.id
      state.name = payload.name
      state.cover = payload.cover
      state.tracks = payload.tracks
      state.children = payload.children
      state.liked = payload.liked
    },
    setLike(state, payload) {
      const { like } = payload
      state.liked = like
    },
    setLikedAlbums(state, payload) {
      state.likedAlbums = payload
    },
  },
  actions: {
    async loadRandomAlbum(context) {
      const token = context.rootGetters['auth/accessToken']
      const album = await flowChartApi.fetchRandomAlbum(token);
      context.commit('setAlbum', album);
    },
    async loadAlbum(context, payload) {
      const token = context.rootGetters['auth/accessToken']
      const album = await flowChartApi.fetchAlbum(payload.id, token)
      context.commit('setAlbum', album)
    },
    async likeAlbum(context, payload) {
      const { id }  = payload
      const token = context.rootGetters['auth/accessToken']

      await flowChartApi.likeAlbum(id, token)
      context.commit('setLike', {like: true})
    },
    async dislikeAlbum(context, payload) {
      const { id }  = payload
      const token = context.rootGetters['auth/accessToken']

      await flowChartApi.dislikeAlbum(id, token)
      context.commit('setLike', {like: false})
    },
    async loadLikedAlbums(context) {
      const accessToken = context.rootGetters['auth/accessToken']
      const data = await flowChartApi.fetchLikedAlbums(accessToken)
      context.commit('setLikedAlbums', data)
    },
  }
};
