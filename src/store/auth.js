import { flowChartApi } from '@/api';
import { dropTokens, getExpiration, saveTokens } from '@/auth';


export default {
  namespaced: true,
  state() {
    return {
      isAuthenticated: false,

      username: null,
      email: null,

      accessToken: null,
      refreshToken: null,

      _heartbeat: null,
    }
  },
  getters: {
    accessToken: state => state.accessToken,
  },
  mutations: {
    setAuthenticated(state, payload) {
      state.isAuthenticated = payload
    },
    setUser(state, payload) {
      state.username = payload.username
      state.email = payload.email
    },
    setTokens(state, payload) {
      state.accessToken = payload.access
      state.refreshToken = payload.refresh
      if (payload.persist) {
        saveTokens(payload)
      }
    },
    setHeartbeat(state, payload) {
      state._heartbeat = payload
    },
    dropTokens(state) {
      state.isAuthenticated = false
      state.accessToken = null
      state.refreshToken = null

      dropTokens()

      if (state._heartbeat !== null) {
        clearTimeout(state._heartbeat)
        state._heartbeat = null
      }
    },
  },
  actions: {
    async register(context, payload) {
      await flowChartApi.registerUser(payload)
    },

    async login(context, payload) {
      const data = await flowChartApi.obtainJWTToken({
        username: payload.username,
        password: payload.password,
      })

      context.commit('setTokens', {
        ...data,
        persist: true,
      })

      context.commit('setAuthenticated', true)

      await context.dispatch('fetchUserInfo')
      await context.dispatch('_startAuthHeartbeat')
    },

    async initAuth(context, payload) {
      context.commit('setTokens', payload)
      await context.dispatch('_startAuthHeartbeat')
      await context.dispatch('fetchUserInfo')
    },

    async fetchUserInfo(context) {
      const data = await flowChartApi.fetchUserInfo(context.state.accessToken)
      context.commit('setUser', data)
    },

    async _startAuthHeartbeat(context) {
      console.log('heartbeat')
      const access = context.state.accessToken
      const refresh = context.state.refreshToken
      const REQUEST_GAP = 1000  // ms

      if (!refresh) {
        context.commit('dropTokens')
        context.commit('setAuthenticated', false)
      }

      const accessExpiresIn = Math.max(getExpiration(access) - REQUEST_GAP, 0)

      // access didn't expire
      if (accessExpiresIn > 0) {
        context.commit('setAuthenticated', true)

        const timerId = setTimeout(async () => {
          await context.dispatch('_startAuthHeartbeat')
        }, accessExpiresIn)

        context.commit('setHeartbeat', timerId)
      }
      // refresh didn't expire
      else if (getExpiration(refresh) > 0) {
        const data = await flowChartApi.refreshJWTToken(refresh)

        context.commit('setTokens', {
          ...data,
          persist: true,
        })
        context.commit('setAuthenticated', true)

        const newAccessExpiresIn = getExpiration(data.access)

        const timerId = setTimeout(async () => {
          await context.dispatch('_startAuthHeartbeat')
        }, newAccessExpiresIn)

        context.commit('setHeartbeat', timerId)
      }
      // tokens has expired
      else {
        context.commit('dropTokens')
        context.commit('setAuthenticated', false)
      }
    },
  }
};
