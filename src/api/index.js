import axios from 'axios'
import { API_HOST } from '@/config';

class FlowChartApi {
  constructor(url) {
    this.url = url
  }

  async fetchRandomAlbum(accessToken) {
    return await this.request({
      path: '/albums/random',
      method: 'get',
      auth: accessToken,
    })
  }

  async fetchAlbum(id, accessToken) {
    return await this.request({
      path: `/albums/${id}`,
      method: 'get',
      auth: accessToken,
    })
  }

  async likeAlbum(id, accessToken) {
    return await this.request({
      path: `/albums/${id}/like`,
      method: 'post',
      auth: accessToken
    })
  }

  async dislikeAlbum(id, accessToken) {
    return await this.request({
      path: `/albums/${id}/dislike`,
      method: 'post',
      auth: accessToken
    })
  }

  async fetchLikedAlbums(accessToken) {
    return await this.request({
      path: `/albums/liked`,
      method: 'get',
      auth: accessToken
    })
  }

  async registerUser(userData) {
    return await this.request({
      path: '/auth/register',
      method: 'post',
      data: userData,
    })
  }

  async obtainJWTToken(loginData) {
    return await this.request({
      path: '/auth/token/obtain',
      method: 'post',
      data: loginData,
    })
  }

  async refreshJWTToken(refreshToken) {
    return await this.request({
      path: '/auth/token/refresh',
      method: 'post',
      data: { refresh: refreshToken },
    })
  }

  async fetchUserInfo(accessToken) {
    return await this.request({
      path: '/auth/info',
      method: 'get',
      auth: accessToken,
    })
  }

  async request({ path, method, data, auth }) {
    const url = this.url + path;

    const headers = auth ? { Authorization: `Bearer ${auth}` } : { }

    const response = await axios({
      method,
      url,
      data,
      headers,
    })
    return await response.data
  }
}

export const flowChartApi = new FlowChartApi(API_HOST)
