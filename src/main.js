import { createApp } from 'vue'
import App from '@/App.vue'
import router from '@/router';
import store from '@/store/index';
import AudioPlayer from '@/components/player/AudioPlayer';
import NavigationBar from '@/components/navigation/NavigationBar';



const app = createApp(App)

app.use(router)
app.use(store)

app.component('audio-player', AudioPlayer);
app.component('navigation-bar', NavigationBar);


app.mount('#app')
